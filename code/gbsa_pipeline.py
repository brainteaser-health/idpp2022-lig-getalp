import os
import json
import joblib
import numpy as np
import pandas as pd
from collections import defaultdict
from functools import partial
from argparse import ArgumentParser
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sksurv.ensemble import GradientBoostingSurvivalAnalysis
from sksurv.metrics import as_concordance_index_ipcw_scorer
from xgboost import XGBRegressor
from lifelines.utils import concordance_index

from util import *


def run_gridsearch(X, y, params, nsplits, outdir, sample_weights):
    surv_model = GradientBoostingSurvivalAnalysis()
    gridsearch = GridSearchCV(as_concordance_index_ipcw_scorer(surv_model), params, cv=nsplits, sample_weights=sample_weights)
    gridsearch.fit(X, y)
    joblib.dump(gridsearch.best_estimator_.estimator_, os.path.join(outdir, 'survival_analysis_model.joblib'))
    pd.DataFrame(gridsearch.cv_results_).to_csv(os.path.join(outdir, 'param_search_cv_results.csv'))
    with open(os.path.join(outdir, 'selected_params.json'), 'w') as f:
        json.dump(gridsearch.best_params_, f)
    return gridsearch


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('dataset_id', type=str, choices={'a', 'b', 'c'})
    parser.add_argument('--t0', action='store_true')
    parser.add_argument('--agg', type=str, choices={'mean', 'max', 'last', 'twa'}, default='mean')
    parser.add_argument('--alsfrs_level', type=int, default=0)
    parser.add_argument('--thr_risk', action='store_true')
    parser.add_argument('--cv_folds', type=int, default=5)
    parser.add_argument('--lr', type=float)
    parser.add_argument('--max_depth', type=int)
    parser.add_argument('--n_est', type=int)
    parser.add_argument('--weight_samples', action='store_true')
    parser.add_argument('--silent', action='store_true')
    return parser.parse_args()


def main(args):
    output_dir, datadir, dataset_name = make_run_dirs(args.dataset_id, args.t0, 'gbsa')
    with open(os.path.join(output_dir, 'script_clargs.json'), 'w') as f:
        json.dump(vars(args), f)

    if not args.silent:
        print('Loading data...')
    static_df = pd.read_csv(os.path.join(datadir, 'prepped-train', dataset_name, 'static.csv'), index_col='id')
    alsfrs_df = pd.read_csv(os.path.join(datadir, 'prepped-train', dataset_name, 'alsfrs.csv'), index_col='id')
    outcome_df = pd.read_csv(
        os.path.join(datadir, 'original', f'{dataset_name}_train-outcome.csv'), index_col='PatientID'
    )

    if not args.silent:
        print('Preparing data...')
    if args.t0:
        alsfrs_df = alsfrs_df[alsfrs_df.date_alsfrs_r == 0.]
    else:
        if args.agg == 'twa':
            date_coef = np.exp(-alsfrs_df.date_alsfrs_r.values)
            alsfrs_df = alsfrs_df.apply(lambda x: x * date_coef if x.name != 'date_alsfrs_r' else x) \
                .groupby('id').mean()
        elif args.agg == 'last':
            alsfrs_df = alsfrs_df.groupby('id').max()
        else:
            alsfrs_df = alsfrs_df.groupby('id').agg(args.agg)
        # for now, ignore spirometry data
        # spiro_df = pd.read_csv(os.path.join(datadir, 'prepped', dataset_name, 'spiro.csv'), index_col='id')
        # spiro_df = spiro_df.drop('date_spiro', axis=1).groupby('id').agg(args.agg)

    alsfrs_df = filter_alsfrs_data(alsfrs_df, args.alsfrs_level)

    x_df = static_df.join(alsfrs_df)
    # try:
    #     x_df = x_df.join(spiro_df)
    # except NameError:
    #     pass
    full_df = x_df.join(outcome_df)

    params = {}
    if args.lr is None:
        params['estimator__learning_rate'] = [a * 10 ** b for b in range(-5, -1) for a in [1, 2, 5]]
    if args.max_depth is None:
        params['estimator__max_depth'] = [3, 5, 10]
    if args.n_est is None:
        params['estimator__n_estimators'] = [50, 100, 150, 200]
    
    if args.dataset_id == 'c':
        input_df = full_df.drop(outcome_df.columns, axis=1)  # outcome table was only joined to ensure label ordering
        X = input_df.values
        full_df['y_event'] = full_df.Type.apply(lambda x: x == 'DEATH').astype(int)
        if args.weight_samples:
            class_weights = len(full_df) / (2 * np.bincount(full_df.y_event.values))
            sample_weights = np.fromiter((class_weights[i] for i in full_df.y_event.values), dtype=np.float32)
        else:
            sample_weights = None
        y_2d = full_df.loc[:, ['y_event', 'Time']].values
        y_input = prep_surv_model_input(y_2d)
        if len(params):
            if not args.silent:
                print('Hyperparameter search...')
            gridsearch = run_gridsearch(X, y_input, params, args.cv_folds, output_dir, sample_weights)
            chosen_params = {k.replace('estimator__', ''): v for k, v in gridsearch.best_params_.items()}
            if not args.silent:
                params2print = ', '.join([f'{k}={v}' for k, v in chosen_params.items()])
                print(f'Selected hyperparameters; ' + params2print)
        else:
            chosen_params = dict(learning_rate=args.lr, max_depth=args.max_depth, n_estimators=args.n_est)
    else:
        competing_event = 'PEG' if args.dataset_id == 'b' else 'NIV'
        classes = ('NONE', competing_event, 'DEATH')
        full_df['y_event'] = full_df.Type.apply(lambda x: classes.index(x))
        input_df_1 = full_df[full_df.y_event < 2]
        input_df_2 = full_df[full_df.y_event != 1]
        if args.weight_samples:
            class_weights1 = len(input_df_1) / (2 * np.bincount(input_df_1.y_event.values))
            sample_weights1 = np.fromiter((class_weights1[i] for i in input_df_1.y_event.values), dtype=np.float32)
            class_weights2 = len(input_df_2) / (2 * np.bincount(input_df_2.y_event.values))
            sample_weights2 = np.fromiter((class_weights2[i] for i in input_df_2.y_event.values), dtype=np.float32)
        else:
            sample_weights1 = sample_weights2 = None
        cols_to_drop = outcome_df.columns.tolist() + ['y_event']
        X1 = input_df_1.drop(cols_to_drop, axis=1).values
        X2 = input_df_2.drop(cols_to_drop, axis=1).values
        y_input1 = prep_surv_model_input(input_df_1.loc[:, ['y_event', 'Time']].values)
        y_input2 = prep_surv_model_input(input_df_2.loc[:, ['y_event', 'Time']].values)
        if len(params):
            if not args.silent:
                print('Hyperparameter search...')
            output_subdir1 = os.path.join(output_dir, competing_event.lower())
            os.mkdir(output_subdir1)
            gridsearch1 = run_gridsearch(X1, y_input1, params, args.cv_folds, output_subdir1, sample_weights1)
            chosen_params1 = {k.replace('estimator__', ''): v for k, v in gridsearch1.best_params_.items()}
            if not args.silent:
                params2print = ', '.join([f'{k}={v}' for k, v in chosen_params1.items()])
                print(f'{competing_event}: selected hyperparameters; ' + params2print)
            output_subdir2 = os.path.join(output_dir, 'death')
            os.mkdir(output_subdir2)
            gridsearch2 = run_gridsearch(X2, y_input2, params, args.cv_folds, output_subdir2, sample_weights2)
            chosen_params2 = {k.replace('estimator__', ''): v for k, v in gridsearch2.best_params_.items()}
            if not args.silent:
                params2print = ', '.join([f'{k}={v}' for k, v in chosen_params2.items()])
                print(f'DEATH: selected hyperparameters; ' + params2print)
        else:
            chosen_params1 = chosen_params2 = dict(learning_rate=args.lr, max_depth=args.max_depth, n_estimators=args.n_est)

    ## EVAL CHOSEN MODEL AS A CLASS PREDICTOR FOR T1
    if not args.silent:
        print('Running evaluation metrics for Task 1...')
    cv_metrics = defaultdict(list)
    skf_cv = StratifiedKFold(n_splits=args.cv_folds, shuffle=True, random_state=0)
    if args.dataset_id == 'c':
        for train_idx, eval_idx in skf_cv.split(np.empty_like(y_input), y_2d[:, 0]):
            train_data = X[train_idx], y_input[train_idx]
            X_eval, y_eval = X[eval_idx], y_input[eval_idx]
            model = GradientBoostingSurvivalAnalysis(**chosen_params).fit(*train_data, sample_weights=sample_weights[train_idx])
            cv_metrics['c_index'].append(model.score(X_eval, y_eval))
            risks_train = model.predict(train_data[0])
            risks_eval = model.predict(X_eval)
            probs = stretch_sigmoid(risks_eval, calculate_sigmoid_flex_param(risks_train))
            t = 1 - y_2d[train_idx, 0].mean() if args.thr_risk else .5
            preds = np.apply_along_axis(lambda x: x > t, 0, probs).astype(int)
            y_eval_classes = y_2d[eval_idx, 0]
            cv_metrics['precision'].append(metrics.precision_score(y_eval_classes, preds))
            cv_metrics['recall'].append(metrics.recall_score(y_eval_classes, preds))
            cv_metrics['f1'].append(metrics.f1_score(y_eval_classes, preds))
            cv_metrics['auroc'].append(metrics.roc_auc_score(y_eval_classes, risks_eval))
    else:
        X = full_df.drop(cols_to_drop, axis=1).values
        y_events = full_df.y_event.values
        for train_idx, eval_idx in skf_cv.split(np.empty_like(y_events), y_events):
            X_train, y_train_classes = X[train_idx], y_events[train_idx]
            X_eval, y_eval_classes = X[eval_idx], y_events[eval_idx]
            X_train1 = X_train[y_train_classes < 2]
            sw_idx1 = train_idx & (y_train_classes < 2)
            y_train1 = prep_surv_model_input(full_df.iloc[train_idx, :][y_train_classes < 2].loc[:, ['y_event', 'Time']].values)
            model1 = GradientBoostingSurvivalAnalysis(**chosen_params1).fit(X_train1, y_train1, sample_weights=sample_weights1[sw_idx1])
            risks_train1 = model1.predict(X_train)
            prob_func1 = partial(stretch_sigmoid, a=calculate_sigmoid_flex_param(risks_train1))
            y_eval1 = prep_surv_model_input(full_df.iloc[eval_idx, :][y_eval_classes < 2].loc[:, ['y_event', 'Time']].values)
            cv_metrics[f'c_index_{competing_event.lower()}'].append(model1.score(X_eval[y_eval_classes < 2], y_eval1))
            X_train2 = X_train[y_train_classes != 1]
            sw_idx2 = train_idx & (y_train_classes != 1)
            y_train2 = prep_surv_model_input(full_df.iloc[train_idx, :][y_train_classes != 1].loc[:, ['y_event', 'Time']].values)
            model2 = GradientBoostingSurvivalAnalysis(**chosen_params2).fit(X_train2, y_train2, sample_weights=sample_weights2[sw_idx2])
            risks_train2 = model2.predict(X_train)
            prob_func2 = partial(stretch_sigmoid, a=calculate_sigmoid_flex_param(risks_train2))
            y_eval2 = prep_surv_model_input(full_df.iloc[eval_idx, :][y_eval_classes != 1].loc[:, ['y_event', 'Time']].values)
            cv_metrics['c_index_death'].append(model2.score(X_eval[y_eval_classes != 1], y_eval2))
            preds, probs = two_outcome_prediction(X_eval, model1, model2, prob_func1, prob_func2, not args.thr_risk)
            cv_metrics['micro_precision'].append(metrics.precision_score(y_eval_classes, preds, average='micro'))
            cv_metrics['micro_recall'].append(metrics.recall_score(y_eval_classes, preds, average='micro'))
            cv_metrics['micro_f1'].append(metrics.f1_score(y_eval_classes, preds, average='micro'))
            cv_metrics['weighted_precision'].append(metrics.precision_score(y_eval_classes, preds, average='weighted'))
            cv_metrics['weighted_recall'].append(metrics.recall_score(y_eval_classes, preds, average='weighted'))
            cv_metrics['weighted_f1'].append(metrics.f1_score(y_eval_classes, preds, average='weighted'))
            cv_metrics['auroc'].append(metrics.roc_auc_score(y_eval_classes, probs, average='weighted', multi_class='ovo'))
    cv_metrics_avg = {k: sum(v) / len(v) for k, v in cv_metrics.items()}
    with open(os.path.join(output_dir, 'evaluation_metrics_T1.txt'), 'w') as f:
        f.write(f'== EVALUATION METRICS ==\nTotal N={len(full_df)}, CV eval sets N={len(eval_idx)}\n')
        for k, v in cv_metrics_avg.items():
            f.write(f'{k} = {round(v, 4)}\n')
    if not args.silent:
        print('Event prediction:', end=' ')
        for k, v in cv_metrics_avg.items():
            print(f'{k}={round(v, 3)}', end='  ')
        print('\nT2 hyperparameter search...')

    # TIME TO EVENT PREDICTION
    y_times = full_df.Time.values
    y_classes = convert_times_to_prediction_windows(y_times, full_df.Type.values, True)
    regression_gridsearch_params = dict(
        learning_rate=[a * 10 ** b for b in range(-5, -1) for a in [1, 2, 5]],
        max_depth=[3, 5, 10, 15],
        n_estimators=[100, 150, 200]
    )
    regression_gridsearch = GridSearchCV(XGBRegressor(), regression_gridsearch_params, scoring='neg_mean_absolute_error')
    regression_gridsearch.fit(X, y_times)
    chosen_params_regression = regression_gridsearch.best_params_
    if not args.silent:
        params2print = ', '.join([f'{k}={v}' for k, v in chosen_params_regression.items()])
        print(f'Survival time regression: selected hyperparameters; ' + params2print)
        print('Running evaluation metrics for Task 2...')
    joblib.dump(regression_gridsearch.best_estimator_, os.path.join(output_dir, 'regressor.joblib'))
    cv_metrics = defaultdict(list)
    for train_idx, eval_idx in skf_cv.split(np.empty_like(y_times), y_classes):
        X_train = X[train_idx]
        X_eval, y_eval_classes = X[eval_idx], y_classes[eval_idx]
        regressor = XGBRegressor(**chosen_params_regression).fit(X_train, y_times[train_idx])
        survival_times = regressor.predict(X_eval)
        cv_metrics['c_index'].append(concordance_index(y_times[eval_idx], survival_times))
        if args.dataset_id == 'c':
            surv_model = GradientBoostingSurvivalAnalysis(**chosen_params).fit(X_train, y_input[train_idx])
            risks_train = surv_model.predict(X_train)
            risks_eval = surv_model.predict(X_eval)
            probs = stretch_sigmoid(risks_eval, calculate_sigmoid_flex_param(risks_train))
            t = 1 - y_2d[train_idx, 0].mean() if args.thr_risk else .5
            event_preds = np.apply_along_axis(lambda x: x > t, 0, probs).astype(int)
        else:
            y_train_events = y_events[train_idx]
            y_surv_train1 = prep_surv_model_input(full_df.iloc[train_idx, :][y_train_events < 2].loc[:, ['y_event', 'Time']].values)
            surv_model1 = GradientBoostingSurvivalAnalysis(**chosen_params1).fit(X_train[y_train_events < 2], y_surv_train1)
            risks_train = surv_model1.predict(X_train)
            risks_eval1 = surv_model1.predict(X_eval)
            probs1 = stretch_sigmoid(risks_eval1, calculate_sigmoid_flex_param(risks_train1))
            thr_idx1 = probs1 < .5
            y_surv_train2 = prep_surv_model_input(full_df.iloc[train_idx, :][y_train_events != 1].loc[:, ['y_event', 'Time']].values)
            surv_model2 = GradientBoostingSurvivalAnalysis(**chosen_params2).fit(X_train[y_train_events != 1], y_surv_train2)
            risks_train2 = surv_model2.predict(X_train)
            risks_eval2 = surv_model2.predict(X_eval)
            probs2 = stretch_sigmoid(risks_eval2, calculate_sigmoid_flex_param(risks_train2))
            thr_idx2 = probs2 < .5
            event_preds, _ = combine_probs(probs1, probs2, thr_idx1, thr_idx2)
        preds = convert_times_to_prediction_windows(survival_times, event_preds, True)
        cv_metrics['micro_precision'].append(metrics.precision_score(y_eval_classes, preds, average='micro'))
        cv_metrics['micro_recall'].append(metrics.recall_score(y_eval_classes, preds, average='micro'))
        cv_metrics['micro_f1'].append(metrics.f1_score(y_eval_classes, preds, average='micro'))
        cv_metrics['weighted_precision'].append(metrics.precision_score(y_eval_classes, preds, average='weighted'))
        cv_metrics['weighted_recall'].append(metrics.recall_score(y_eval_classes, preds, average='weighted'))
        cv_metrics['weighted_f1'].append(metrics.f1_score(y_eval_classes, preds, average='weighted'))
        cv_metrics['macro_precision'].append(metrics.precision_score(y_eval_classes, preds, average='macro'))
        cv_metrics['macro_recall'].append(metrics.recall_score(y_eval_classes, preds, average='macro'))
        cv_metrics['macro_f1'].append(metrics.f1_score(y_eval_classes, preds, average='macro'))
    cv_metrics_avg = {k: sum(v) / len(v) for k, v in cv_metrics.items()}
    with open(os.path.join(output_dir, 'evaluation_metrics_T2.txt'), 'w') as f:
        f.write(f'== EVALUATION METRICS ==\nTotal N={len(y_classes)}, CV eval sets N={len(eval_idx)}\n')
        for k, v in cv_metrics_avg.items():
            f.write(f'{k} = {round(v, 4)}\n')
    if not args.silent:
        print('Time window prediction:', end=' ')
        for k, v in cv_metrics_avg.items():
            print(f'{k}={round(v, 3)}', end='  ')   
        print(f'\nDone! Results in {output_dir}')
    

if __name__ == '__main__':
    args = parse_arguments()
    main(args)

    