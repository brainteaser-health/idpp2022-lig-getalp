import os
import joblib
import numpy as np
import pandas as pd
from argparse import ArgumentParser

from util import filter_alsfrs_data, stretch_sigmoid, convert_times_to_prediction_windows


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('dataset_id', type=str, choices={'a', 'b', 'c'})
    parser.add_argument('training_run_dir', type=str)
    parser.add_argument('thr', type=float, default=.5)
    parser.add_argument('--t0', action='store_true')
    parser.add_argument('--agg', type=str, choices={'mean', 'max', 'last', 'twa'}, default='mean')
    return parser.parse_args()


def main(args):
    here, _ = os.path.split(os.path.realpath(__file__))
    project_dir = os.path.join(here, *[os.pardir for _ in range(2)])
    dataset_name = f'dataset{args.dataset_id.upper()}'
    datadir = os.path.normpath(os.path.join(project_dir, 'data', 'prepped-test', dataset_name))

    alsfrs_df = pd.read_csv(os.path.join(datadir, 'alsfrs.csv'), index_col='id')
    static_df = pd.read_csv(os.path.join(datadir, 'static.csv'), index_col='id')

    if args.t0:
        alsfrs_df = alsfrs_df[alsfrs_df.date_alsfrs_r == 0.]
    else:
        if args.agg == 'twa':
            date_coef = np.exp(-alsfrs_df.date_alsfrs_r.values)
            alsfrs_df = alsfrs_df.apply(lambda x: x * date_coef if x.name != 'date_alsfrs_r' else x) \
                .groupby('id').mean()
        elif args.agg == 'last':
            alsfrs_df = alsfrs_df.groupby('id').max()
        else:
            alsfrs_df = alsfrs_df.groupby('id').agg(args.agg)
    alsfrs_df = filter_alsfrs_data(alsfrs_df, 0)

    x_df = static_df.join(alsfrs_df)
    X = x_df.values
    if args.dataset_id == 'c':
        surv_model = joblib.load(os.path.join(args.training_run_dir, 'survival_analysis_model.joblib'))
        risks = stretch_sigmoid(surv_model.predict(X))
        class_preds_b = np.fromiter((r > args.thr for r in risks), dtype=bool)
        class_preds = np.array(['DEATH' if b else 'NONE' for b in class_preds_b])
    else:
        competing_event = 'niv' if args.dataset_id == 'a' else 'peg'
        surv_model1 = joblib.load(os.path.join(args.training_run_dir, competing_event, 'survival_analysis_model.joblib'))
        surv_model2 = joblib.load(os.path.join(args.training_run_dir, 'death', 'survival_analysis_model.joblib'))
        risks_ = stretch_sigmoid(surv_model1.predict(X)).reshape((-1, 1)), \
            stretch_sigmoid(surv_model2.predict(X)).reshape((-1, 1))
        all_risks = np.hstack(risks_)
        risks = np.fromiter((r.max() for r in all_risks), dtype=np.float32)
        class_preds_n = np.fromiter((0 if r.max() < args.thr else r.argmax() + 1 for r in all_risks), dtype=np.int8)
        classes = 'NONE', competing_event.upper(), 'DEATH'
        class_preds = np.array([classes[i] for i in class_preds_n])
    # from IPython import embed; embed()
    df = pd.DataFrame({'score': risks, 'pred': class_preds}, index=x_df.index)
    df.sort_values('score', ascending=False, inplace=True)
    prev_rs = df.score[0]
    rank_int = 0
    ranking = [rank_int]
    for rs in df.score[1:]:
        if prev_rs > rs:
            rank_int += 1
        ranking.append(rank_int)
    df['ranking'] = ranking
    run_id1 = f'lig-getalp_T1{args.dataset_id}_{"M0" if args.t0 else "M6"}_GBsurv'
    
    with open(os.path.normpath(os.path.join(project_dir, 'idpp2022-lig-getalp', 'submission', 'task1', f'{run_id1}.txt')), 'w') as f:
        for idx, row in df.iterrows():
            f.write(' '.join([idx, str(round(row.score, 4)), str(row.ranking), row.pred, run_id1]) + '\n')

    regressor = joblib.load(os.path.join(args.training_run_dir, 'regressor.joblib'))
    survival_times = regressor.predict(X)
    time_windows = convert_times_to_prediction_windows(survival_times, class_preds)
    df = df.join(pd.DataFrame({'window': time_windows}, index=x_df.index))

    with open(os.path.normpath(os.path.join(project_dir, 'idpp2022-lig-getalp', 'submission', 'task2', f'{run_id1}.txt')), 'w') as f:
        for idx, row in df.iterrows():
            f.write(' '.join([idx, row.window, str(row.ranking), row.pred, run_id1]) + '\n')  


if __name__ == '__main__':
    args = parse_arguments()
    main(args)