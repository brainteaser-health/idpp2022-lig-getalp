import os
import numpy as np
from datetime import datetime
from pandas import read_csv
from scipy.special import softmax


def timestamp_filename(filename, ext=None):
    now = datetime.now()
    timestamp_tmp = now.year, now.month, now.day, now.hour, now.minute
    yr, mon, day, hr, min = map(str, timestamp_tmp)
    dot_idx = filename.rfind('.')
    ext_ = None
    if dot_idx != -1:
        filename, ext_ = filename[:dot_idx], filename[dot_idx:]
    filename_t = '-'.join((filename, yr, mon, day, hr, min))
    if ext is None and ext_ is not None:
        filename_t += ext_
    elif ext is not None:
        filename_t += ext

    return filename_t


def make_run_dirs(dataset_id, t0, suffix):
    here, _ = os.path.split(os.path.realpath(__file__))
    project_dir = os.path.join(here, *[os.pardir for _ in range(2)])
    datadir = os.path.normpath(os.path.join(project_dir, 'data'))
    dataset_name = f'dataset{dataset_id.upper()}'
    dirname = f'dataset{dataset_id}_M{0 if t0 else 6}_{suffix}'
    output_subdir_name = timestamp_filename(dirname)
    output_dir = os.path.normpath(os.path.join(project_dir, 'runs', output_subdir_name))
    os.mkdir(output_dir)

    return output_dir, datadir, dataset_name


def stretch_sigmoid(x, a=2):
    return (1. + np.exp(-a * np.log(2 + 3 ** .5) * x)) ** -1


def prep_surv_model_input(y):
    return np.fromiter(zip(y[:, 0], y[:, 1]), dtype=[('event', bool), ('time', 'f4')])


def make_preds(X, model, prob_fn, thr):
    pseudo_probs = prob_fn(model.predict(X))
    thr = thr if thr else pseudo_probs.mean()
    preds_bool_neg = pseudo_probs < thr
    return pseudo_probs, preds_bool_neg


def two_outcome_prediction(X, model1, model2, prob_fn1, prob_fn2, thr):
    thr = .5 if thr else None
    preds1, thr_idx1 = make_preds(X, model1, prob_fn1, thr)
    preds2, thr_idx2 = make_preds(X, model2, prob_fn2, thr)
    return combine_probs(preds1, preds2, thr_idx1, thr_idx2)
    

def combine_probs(preds1, preds2, thr_idx1, thr_idx2):
    neg_probs_avg = ((1 - preds1) + (1 - preds2)) / 2
    probs = softmax((neg_probs_avg, preds1, preds2), axis=0).T
    preds = np.empty_like(neg_probs_avg)
    pred_idx1 = preds1 > preds2
    preds[pred_idx1] = np.ones(pred_idx1.sum())
    pred_idx2 = preds1 < preds2
    preds[pred_idx2] = 2 * np.ones(pred_idx2.sum())
    pred_idx_neither = thr_idx1 & thr_idx2
    preds[pred_idx_neither] = np.zeros(pred_idx_neither.sum())
    return preds.astype(int), probs


def calculate_sigmoid_flex_param(risks):
    return (abs(risks.max() ** -1)  + abs(risks.min() ** -1)) / 2


def calculate_survival_times(probs, funcs):
    return np.fromiter((f.x[np.abs(f.y - p).argmin()] for p, f in zip(probs, funcs)), dtype=np.float32)


def convert_times_to_prediction_windows(times, types=None, as_ints=False):
    classes = '6-12', '12-18', '18-24', '24-30', '30-36', '>36'
    def _getwin(i):
        return i if as_ints else classes[i]
    def _bucket(t):
        if t <= 12:
            return _getwin(0)
        elif t <= 18:
            return _getwin(1)
        elif t <= 24:
            return _getwin(2)
        elif t <= 30:
            return _getwin(3)
        elif t <= 36:
            return _getwin(4)
        else:
            return _getwin(5)
    ret = []
    if types is not None:
        none_val = 0 if types.dtype == int else 'NONE'
        for time, type in zip(times, types):
            if type == none_val:          
                ret.append(_getwin(5))
            else:
                ret.append(_bucket(time))
    else:
        for time in times:
            ret.append(_bucket(time))
    return np.array(ret)


def filter_alsfrs_data(df, level):
    if level < 1:
        return df.drop([c for c in df if len(c) > 3], axis=1)
    elif level == 1:
        return df.loc[:,
            ['bulbar_subscore', 'fine_motor_subscore', 'gross_motor_subscore', 'respiratory_subscore']
        ]
    elif level == 2:
        return df.loc[:, ['bulbar_subscore', 'motor_subscore', 'respiratory_subscore']]
    else:
        return df.alsfrs_r_tot_score