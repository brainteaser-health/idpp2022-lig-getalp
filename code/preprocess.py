import pandas as pd
import numpy as np
import os
from argparse import ArgumentParser
from warnings import simplefilter


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('dataset_id', type=str, choices={'a', 'b', 'c'})
    parser.add_argument('--impute', action='store_true')
    parser.add_argument('--test', action='store_true')
    return parser.parse_args()


def main(args):
    here, _ = os.path.split(os.path.realpath(__file__))
    datadir = os.path.normpath(os.path.join(here, '..', '..', 'data'))
    dataset_name = f'dataset{args.dataset_id.upper()}'
    input_dir = os.path.join(datadir, 'original')
    div = 'test' if args.test else 'train'
    output_dir = os.path.join(datadir, 'prepped-' + div)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    output_subdir = os.path.join(output_dir, dataset_name)
    if not os.path.isdir(output_subdir):
        os.mkdir(output_subdir)

    visits_df = pd.read_csv(os.path.join(input_dir, dataset_name + f'_{div}-visits.csv'), index_col='id')
    spiro_df = (visits_df[~pd.isna(visits_df.fvcValue)]
                .drop([c for c in visits_df.columns if c not in ('date_spiro', 'fvcValue')], axis=1))
    spiro_df.to_csv(os.path.join(output_subdir, 'spiro.csv'))
    alsfrs_df = visits_df[~pd.isna(visits_df.alsfrs_r_tot_score)].drop(['date_spiro', 'fvcValue'], axis=1)
    alsfrs_df['fine_motor_subscore'] = alsfrs_df.q4 + alsfrs_df.q5 + alsfrs_df.q6
    alsfrs_df['gross_motor_subscore'] = alsfrs_df.q7 + alsfrs_df.q8 + alsfrs_df.q9
    alsfrs_df.to_csv(os.path.join(output_subdir, 'alsfrs.csv'))

    static_df = pd.read_csv(
        os.path.join(input_dir, dataset_name + f'_{div}-static-vars.csv'),
        usecols=[
            'id', 'onsetDate', 'diagnosisDate', 'sex', 'height', 'weight_before_onset', 'weight',
            'moreThan10PercentWeightloss', 'major_trauma_before_onset', 'surgical_interventions_before_onset',
            'age_onset', 'mixedMN', 'onset_bulbar', 'onset_axial', 'onset_limb_type', 'retired_at_diagnosis',
            'ALS_familiar_history', 'smoking', 'turin_C9orf72_kind', 'hypertension', 'diabetes', 'dyslipidemia',
            'thyroid_disorder', 'autoimmune_disease', 'stroke', 'cardiac_disease', 'primary_neoplasm', 'slope'
        ],
        index_col='id'
    )
    if args.impute:
        cols_to_impute = ['weight', 'height', 'weight_before_onset']
        data_to_impute = static_df.loc[:, cols_to_impute]
        try:
            os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # silence tf messages
            import MIDASpy as md
            import tensorflow as tf
            imputer = md.Midas(layer_structure=[128, 128, 128], seed=0, savepath=os.path.join(datadir, 'imputers'))
            imputer.build_model(data_to_impute, verbose=False)
            imputer.train_model(training_epochs=16, verbose=False)
            with tf.compat.v1.Session(graph=imputer.graph) as sess:
                imputer.saver.restore(sess, imputer.savepath)
                feed_dict = {imputer.X: imputer.imputation_target.values}
                imputed_vals = sess.run(imputer.output_op, feed_dict=feed_dict)
        except Exception:
            from sklearn.experimental import enable_iterative_imputer
            from sklearn.impute import IterativeImputer
            from sklearn.exceptions import ConvergenceWarning
            simplefilter(action='ignore', category=ConvergenceWarning)
            imputed_vals = IterativeImputer().fit_transform(data_to_impute.values)
        static_df = static_df.assign(**{
            cols_to_impute[i]: imputed_vals[:, i] for i in range(imputed_vals.shape[1])
        })
    static_df['bmi_change'] = (static_df.weight_before_onset - static_df.weight) * static_df.height ** -2
    static_df.drop(['weight', 'height', 'weight_before_onset'], axis=1, inplace=True)
    onset_limb_type_data = static_df.onset_limb_type.fillna('').apply(lambda x: x.replace('limb', ''))
    onset_limb_types = 'upper', 'lower', 'distal', 'proximal', 'left', 'right'
    onset_limb_type_enc = np.zeros((len(static_df), 6), dtype=np.int8)
    for i, elem in enumerate(onset_limb_type_data):
        for type_ in elem.split('-'):
            try:
                j = onset_limb_types.index(type_)
            except ValueError:
                continue
            onset_limb_type_enc[i, j] += 1
    static_df.drop(['onset_limb_type'], axis=1, inplace=True)
    for j, onset_limb_type in enumerate(onset_limb_types):
        static_df['onset_limb_' + onset_limb_type] = onset_limb_type_enc[:, j]
    static_df.ALS_familiar_history = static_df.ALS_familiar_history.apply(lambda x: x == 'Relative_with_ALS').astype(int)
    turin_C9orf72_kind_enc = {np.nan: 0, 'A': 1, 'B': 2}
    static_df.turin_C9orf72_kind = static_df.turin_C9orf72_kind \
        .apply(lambda x: turin_C9orf72_kind_enc[x])
    static_df.sex = static_df.sex.apply(lambda x: x == 'Female').astype(int)
    for col in static_df:
        if static_df.dtypes[col] == bool:
            static_df[col] = static_df[col].astype(int)
    other_bool_cols = (
        'moreThan10PercentWeightloss', 'major_trauma_before_onset', 'surgical_interventions_before_onset',
        'mixedMN','retired_at_diagnosis', 'smoking', 'hypertension', 'diabetes', 'dyslipidemia',
        'thyroid_disorder', 'autoimmune_disease', 'stroke', 'cardiac_disease', 'primary_neoplasm'
    )
    for obc in other_bool_cols:
        static_df[obc] = static_df[obc].fillna(False).astype(int)
    static_df.to_csv(os.path.join(output_subdir, 'static.csv'))
                
    
if __name__ == '__main__':
    args = parse_arguments()
    main(args)